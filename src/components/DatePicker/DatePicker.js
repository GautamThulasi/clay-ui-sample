import React, {useState} from 'react';
import '@clayui/css/lib/css/atlas.css';
import ClayDatePicker from '@clayui/date-picker';
const spritemap = require('@clayui/css/lib/images/icons/icons.svg');

 const DatePicker = (props) => {
	const todayDate= (new Date()).toLocaleDateString('fr-CA').split(',')[0]
	const [ value, setValue ] = useState(todayDate);
	const [expanded, setExpanded]= useState(false)
		return (
			<>
				<ClayDatePicker
					expanded={expanded}
					onExpandedChange={setExpanded}
					onValueChange={(value, type) => {
						setValue(convertToValidDateFormat(value));
						if (type === 'click') {
							setExpanded(false);
						}
					}}
					placeholder="YYYY-MM-DD"
					spritemap={spritemap}
					value={value}
					years={{
						end: 2024,
						start: 1997,
					}}
				/>
			</>
		)
				}
				export default DatePicker

				const convertToValidDateFormat=(date)=>((new Date(Date.parse(date))).toLocaleDateString('fr-CA').split(',')[0])