import React, { useState } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
const ReactDatePicker = ({ value }) => {
	const [ startDate, setStartDate ] = useState(value ? new Date(value) : new Date());
	return (
		<DatePicker
			selected={startDate}
			onChange={(date) => {
				setStartDate(date);
			}}
		/>
	);
};

export default ReactDatePicker;
