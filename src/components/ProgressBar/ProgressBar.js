import React, { useState } from 'react';
import ClayMultiStepNav from '@clayui/multi-step-nav';
// Imports the @clayui/css package CSS
import '@clayui/css/lib/css/atlas.css';

// You will use this to move to some components
// that need Clay icons, you can host your own
// set of icons in your application, we are using
// CDN to make it easier.
const spritemap = 'https://cdn.jsdelivr.net/npm/@clayui/css/lib/images/icons/icons.svg';

const ProgressBar = ({ activeIndex, progressBarConfig }) => {
	const steps = progressBarConfig.map((item, key) => ({
		active: key === activeIndex,
		complete: key < activeIndex,
		subTitle: item
	}));

	return (
		<ClayMultiStepNav>
			{steps.map(({ active, complete, onClick, subTitle, title }, i) => (
				<ClayMultiStepNav.Item active={active} complete={complete} expand={i + 1 !== steps.length} key={i}>
					<ClayMultiStepNav.Title>{title}</ClayMultiStepNav.Title>
					<ClayMultiStepNav.Divider />
					<ClayMultiStepNav.Indicator
						complete={complete}
						label={1 + i}
						spritemap={spritemap}
						subTitle={subTitle}
					/>
				</ClayMultiStepNav.Item>
			))}
		</ClayMultiStepNav>
	);
};

export default ProgressBar;
