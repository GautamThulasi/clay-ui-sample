import React, { useState } from 'react';
import { ClayInput } from '@clayui/form';
import '@clayui/css/lib/css/atlas.css';

const FormInput = () => <ClayInput id="basicInputText" placeholder="Insert your name here" type="text" />;
export default FormInput;
