import React, { useState } from 'react';
import { ClaySelect } from '@clayui/form';
import '@clayui/css/lib/css/atlas.css';

const FormSelect = ({ options }) => (
	<ClaySelect aria-label="Select Label" id="mySelectId">
		{options.map((item) => <ClaySelect.Option key={item.value} label={item.label} value={item.value} />)}
	</ClaySelect>
);
export default FormSelect;
