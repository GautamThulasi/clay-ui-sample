import React from 'react'
import FormInput from './FormInput'
import FormCheckbox from './FormCheckbox'
import FormDatepicker from './FormDatepicker'
import FormRadio from './FormRadio'
import FormSelect from './FormSelect'
import ClayForm from '@clayui/form';



const FormElements=(formProps)=>
{
    return (
        <ClayForm.Group>
        <label htmlFor="basicInputText">Name</label>    
        {{
TextBox: <FormInput {...formProps}/>,
Select:<FormSelect {...formProps}/>,
Radio:<FormRadio {...formProps}/>,
Datepicker:<FormDatepicker {...formProps}/>,
Checkbox:<FormCheckbox {...formProps}/>,
}[formProps?.type??null]||<div>CREATE NEW COMP</div>}
</ClayForm.Group>
    )
}


export default FormElements;