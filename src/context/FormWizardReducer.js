export const REDUCER = (state, action) => {
	const { payload } = action;
	switch (action.type) {
		case 'UPDATE':
			return {
				...state,
				...payload
			};
		case 'UPDATE_FORM': {
			const { formId, ...rest } = payload;
			const index = state.formIds.indexOf(payload.formId);
			const newForm = { ...state.forms[index], ...rest };
			let formsList = [ ...state.forms ];
			formsList[index] = newForm;
			return {
				...state,
				forms: formsList
			};
		}
		default:
			return state;
	}
};
