import React, { useEffect, useMemo, useState, useReducer, createContext, useCallback } from 'react';
import {REDUCER} from './FormWizardReducer';
import {useContextFactory} from './useContextFactory'
import {getFormWizardProps} from './../services'
const FormWizardContext = createContext();

const FormWizardProvider = (props) => {
	const [ formWizardState, dispatch ] = useReducer(REDUCER, {
		subForms: [],
        formIndex: 0,
        loading:false,
    });
    
    const loadFormConfig = useCallback(async() => {
        try{
           const data = await getFormWizardProps()
           console.log("DADDASASD", data)
        }
        catch(err)
        {
            console.log("ERR!!!!", err)
        }
        }, []);



	useEffect(() => {
            loadFormConfig()
	}, []);

	const value = useMemo(() => {
        return {formWizardState, dispatch}
    }, [ formWizardState ]);
    if(formWizardState?.loading)
    return <div>Loading Indicator</div>

    return <FormWizardContext.Provider value={value}>
{props.children}
    </FormWizardContext.Provider>
};


const useFormWizardState = useContextFactory('FormWizardContext', FormWizardContext)

export { FormWizardProvider, useFormWizardState}