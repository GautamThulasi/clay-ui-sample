import React,{useState, useEffect} from 'react';
import ClayForm, {ClayInput} from '@clayui/form';

export default function Form(props) {
    const [localState, setLocalState]= useState(null)
    const {currentFormProps}= props
    console.log("LOCAL", localState)
    return (
    <>
    <form onChange={(e)=>{e.persist(); setLocalState((prevState)=> { return {...prevState, [e.target.name]: e.target.value}})}}>
        {currentFormProps?.map((formItem, key)=>
        (
            <div style={{margin:20}} key={key}>
                <FormElements {...formItem}/>
            </div>
        ))
        }
        <button onClick={()=>{}} style={{marginLeft:20}}>
Next Page
        </button>
    </form>

    <ClayForm>

    {currentFormProps?.map((formItem, key)=>
        (
            <div style={{margin:20}} key={key}>
                <FormElements {...formItem}/>
            </div>
        ))
        }
    </ClayForm>


    </>
  );
}



const FormElements=(formProps)=>
{
    return ({
TextBox: <input {...formProps}/>,
Select:<SelectComponent {...formProps}/>
}[formProps?.type??null]||<div>CREATE NEW COMP</div>)}

const SelectComponent=(props)=>
{
    const options=
            props.options.map((item, i) => 
   ( <option key={i} value={item.value}>{item.label}</option>))
  return (
      <select {...props}>
          {options}
      </select>
  )
}